# seasons
cross platform linux service deployment, made easy

## context

well, it's that time of development again.  that time of year (or week), where you are building a package.

:phone: ring ring :phone:

:older_man: [manager]: Hello, employee. I hear you finished coding `super-important-project`.  Is that correct?

:bowtie: [you]: Yessir.  I wrote it in [langauge-xyz], so it should work everywhere.

:older_man: [manager]: Great!  In that case I want packages for Ubuntu and RedHat and Suse and Gentoo and Mint and Fedora and Solaris and everything else under-the-sun.

:bowtie: [you]: But si...

:older_man: [mananger]: No buts!  Make it happen.


Well [Bob Sagget](https://www.youtube.com/watch?v=aR7GUiiKEz0&feature=youtu.be&t=13s)!  That sounds like a lot fo work.

## what

- generate *nix services tailored to the target machines init system
  - systemv, upstart, and systemd supported
  -
## how

- against my better judgement... pure `bash`

## refs

- [bash minification](http://bash-minifier.appspot.com/)
- [bash mustache](https://github.com/tests-always-included/mo)
- [bash-templating](https://github.com/johanhaleby/bash-templater/blob/master/templater.sh)
- [init system detection](http://unix.stackexchange.com/questions/18209/detect-init-system-using-the-shell)
